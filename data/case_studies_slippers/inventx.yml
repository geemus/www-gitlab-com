title: Inventx
cover_image: /images/blogimages/cover_image_inventxag_case_study.jpg
cover_title: |
  How GitLab decreased deployment times from 2 days to just 5 minutes for Inventx AG
cover_description: |
  Inventx went from using GitLab strictly for code management to improving customer pipelines, reducing toolchain complexity, and faster bug detection with CI/CD. 
canonical_path: /customers/inventx/
file_name: inventx
customer_logo: /images/case_study_logos/logo-positiv-gray-invent.png
customer_logo_css_class: brand-logo-tall
customer_industry: Technology
customer_location: Chur, Switzerland
customer_solution: GitLab Self-Managed Premium 
customer_employees: 300
customer_overview: |
  Inventx simplified pipelines and increased operational efficiency with GitLab CI/CD.
customer_challenge: |
  Inventx AG was looking for a solution to simplify customer pipelines and reduce toolchain complexity. 
key_benefits: >-
    Faster builds
  
        
    Increased deployments
  
        
    Developer empowerment with code
  
        
    Kubernetes integration
  
        
    Reliable code management
  
        
    Reduced toolchain complexity
  
        
    Jira integration
  
        
    Increased operational efficiency
  
        
    Enhanced bug detections
customer_stats:
  - stat: 20  
    label: releases per day. Up from 4 releases per year.
  - stat: 5     
    label: minute deploys. Down from 2 days.
  - stat: 10-60     
    label: minute bug fixes
customer_study_content:
  - title: the customer
    subtitle: Swiss IT services provider for leading financial institutions
    content: >-
        Inventx AG is a Swiss IT partner for leading financial and insurance service providers. Inventx provides information technology, consulting, and application management services for Switzerland’s prominent financial and insurance organizations. The independent IT company offers individual solutions, the highest quality security, and data protection for its customers.
    
  - title: the challenge
    subtitle:  Simplifying customer complexity
    content: >-
        Inventx AG services various banks with the goal of providing the best possible software solutions. Each customer is treated individually, so various tools are used with different organizations. Powershell scripts, Rundeck Ansible, GitLab, Kubernetes, and Jira are just a few of the tools the teams are accustomed to using.  
      
        
        Inventx wanted to simplify the pipelines for their customers in order to offer smoother workflows and increase operational efficiency. At that time, Inventx counted seven to eight different systems involved in a pipeline at any given time. Ideally, the team would provide customers with a transparent overview, without so much context switching between tools.
  - title: the solution
    subtitle: CI pipeline improvement
    content: >-
        Inventx had experience using GitLab for source code management. In order to simplify workflow, they started using GitLab for CI/CD. “We redesigned the whole pipeline and we're just using GitLab and GitLab runners now. It's all centralized into GitLab and that makes it very easy to troubleshoot. We have a very good overview over all the steps and the customers are also very satisfied with the solution. This really helped us to simplify this task,” said Louis Baumann, DevOps engineer.
      
        
        Inventx created a package service for customers. This is a container platform that includes storage, monitoring, logging services, with the whole CI/CD construct running GitLab. The teams no longer need several physical servers to run pipelines. GitLab has allowed them to minimize their toolchain workflow and context switching between tools. “Using GitLab has reduced complexity enormously, and we accelerated  all our workflow processes. It offers us a great overview of all the processes running for one product. And it simplifies the whole toolchain,” according to Baumann. 
    
      
  - title: the results
    subtitle: Faster builds, increased deployments
    content: >-
        With GitLab, developers are now able to own their own code. “GitLab enables our developers to directly deploy to test environments, to run integration tests on our test environment automatically, and also to deploy some nightly builds to our test environment and test them there, automatically,” Baumann said.
      
        
        Developer production has increased, due to being able to release code on their own. According to Baumann, the team previously was releasing  about four times a year. Now, they’re releasing approximately 20 times a day. “Everything is now contained in GitLab, in one centralized place and  there is one pipeline running where the customer has a great overview of all the different pipelines that were executed,” Baumann said. “If a shot fails, [the customer] can retry at the pipeline, rerun, and has all locks collected on one central project. This is really, really a great value for us and for our customer.”      
      
        
        Engineers are developing on their local machines. Most of the code is written in .NET Core, but they’re starting to write some features in Golang. Teams are using Visual Studio Code as ID, and are deploying using GitLab pipelines to the test environment or the environment from the local machines. The teams execute GitLab runner on Kubernetes; and build their own deployment containers; containing all the functionalities to deploy and manage their resources on Kubernetes.
      
        
        Engineers are able to detect bugs earlier in the software lifecycle because they’re deploying smaller releases, faster. “We're detecting bugs very, very fast and they're also fixed very fast. They can be fixed in maybe 10 minutes to one hour. So bugs are fixed very quickly and also automatically fixed in the production environment. This is a great value,” Baumann added. 
      
        
        GitLab is allowing the teams to have more control, broader visibility, better ability to plan with improved governance. With less tools in the pipelines, customers now have streamlined services.
      
      
      
        
        ## Learn more about Self-Managed Ultimate
      
        
        [Why source code management matters](/stages-devops-lifecycle/source-code-management/)
      
        
        [The benefits of GitLab CI](/stages-devops-lifecycle/continuous-integration/)
      
        
        [Choose a plan that suits your needs](/pricing/)
customer_study_quotes:
  - blockquote: We're detecting bugs very, very fast and they're also fixed very fast. They can be fixed in maybe 10 minutes to one hour. So bugs are fixed very quickly and also automatically fixed in the production environment. This is a great value.
    attribution: Louis Baumann
    attribution_title: DevOps Engineer, Inventx AG
   
  
